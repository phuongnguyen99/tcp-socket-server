const mysql = require("mysql2");

var pool = mysql.createPool({
  connectionLimit: 5,
  host: process.env.HOST || "localhost",
  user: process.env.USER || "root",
  password: process.env.PASS,
  database: "tcp_socket_server",
});

export default db = {
  query: (queryString) => pool.query(queryString),
  add: (table, columns, values) =>
    pool.query(`INSERT INTO ${table} ( ${columns.join()} )
    VALUES (${values.join()})`),
  delete: (table, id) => pool.query(`DELETE FROM ${table} WHERE id = ${id}`),
  pool: () => pool,
};
