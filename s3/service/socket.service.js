import protobufjs from "protobufjs";
import accountPrisma from "../prisma/account.prisma";
import socketPrisma from "../prisma/socket.prisma";
const dataProto = await protobufjs.load("proto/user.proto");
const Account = dataProto.lookupType("userpackage.Account");

const socketService = {};

const encodeAccount = (data) => Account.encode(data).finish();
const decodeAccount = (data) => Account.decode(data);

const defineAccount = async(accountBuf, socketId, callBackError) => {
    const name  = decodeAccount(accountBuf)
    const account = accountPrisma.getAccountByName(name)
    if(!account) callBackError()
    const newData = {
        account_id: account.id,
        socket_id: socketId,
    }
    return socketPrisma.add(newData)
}
export default socketService = {
    encodeAccount,
    decodeAccount,
    defineAccount
}
