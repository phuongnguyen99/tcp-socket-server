import Net from 'net';
import { DEFINE_ACCOUNT_EVENT } from '../socket.event';
import socketService from './service/socket.service';

const port = process.env.PORT_S3 || 8080

const server = new Net.Server()
server.listen(port, () => {
    console.log(`S3 is listening for connection request on socket localhost:${port}`);
})
server.on('connection', (socket) => {
    socket.write('Start new transaction.')
    socket.localAddress
    //fake login
    socket.on(DEFINE_ACCOUNT_EVENT, (data) => {
        socketService.defineAccount(data, soc)
    })
})