import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()
const add = (data) =>  prisma.socket.create(data)

export default socketPrisma = {
    add,
}