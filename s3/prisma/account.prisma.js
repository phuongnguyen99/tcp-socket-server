import { PrismaClient } from '@prisma/client'

const prisma = new PrismaClient()
const getAccountByName = (name) =>  prisma.account.findUnique(name)

export default accountPrisma = {
    getAccountByName,
}